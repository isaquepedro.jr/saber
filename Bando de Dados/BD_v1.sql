
create database saber;

use saber;

CREATE TABLE `Endereço` (
	`ID_Endereço` INT(12) NOT NULL,
    `CEP` INT(8) NOT NULL,
    `Rua` VARCHAR(64) NOT NULL,
    `Numero` INT(5) NOT NULL,
    `Bairro` VARCHAR(32) NOT NULL,
    `Cidade` VARCHAR(32) NOT NULL,
    `Estado` VARCHAR(2) NOT NULL DEFAULT 'PB',
    `País` VARCHAR(32) NOT NULL DEFAULT 'BRASIL',
    PRIMARY KEY (`ID_Endereço`)
);

CREATE TABLE `Contato` (
    `Telefone1` INT(11) NOT NULL,
    `Telefone2` INT(9),
    `Email` VARCHAR(32),
    `ID_Contato` INT(12) NOT NULL AUTO_INCREMENT,
    PRIMARY KEY (`ID_Contato`)
);

CREATE TABLE `Usuário` (
    `Matricula` INT(8) NOT NULL AUTO_INCREMENT,
    `Senha` INT(8) NOT NULL,
    `Nome` VARCHAR(64) NOT NULL,
    `CPF` CHAR(11) NOT NULL UNIQUE,
    `Sexo` ENUM('M', 'F') NOT NULL,
    `Data_Nascimento` DATE NOT NULL,
    `ID_Endereço` INT(12) NOT NULL UNIQUE,
    `ID_Contato` INT(12) NOT NULL UNIQUE,
    `ID_Perfil` ENUM('1', '2', '3') NOT NULL,
    PRIMARY KEY (`Matricula`)
);

CREATE TABLE `Responsável` (
    `Matricula` INT(12) NOT NULL,
    `Mensalidade` DECIMAL(5 , 2 ) NOT NULL,
    `Desconto` DECIMAL(5 , 2 ),
    `ID_Dependente` INT(12) NOT NULL UNIQUE,
    PRIMARY KEY (`Matricula`)
);

CREATE TABLE `Aluno` (
    `ID_Responsável` INT(12) NOT NULL,
    `Matricula` INT(8) NOT NULL,
    `Serie` VARCHAR(32) NOT NULL,
    `Turma` VARCHAR(32) NOT NULL,
    `Mensalidade` DECIMAL(5 , 2 ) NOT NULL,
    PRIMARY KEY (`ID_Responsável` , `Matricula`)
);

CREATE TABLE `Disciplinas` (

    `ID_DIsciplina` INT(12) NOT NULL,
    `Professor` INT(8) NOT NULL,
    `Nome_Disciplina` VARCHAR(30) NOT NULL,
	`Turma` INT (1) NOT NULL,
    `ID_Notas` INT (10) NOT NULL,
    `ID_Frequência` INT(3),
    `Plano de Curso` TEXT,
    PRIMARY KEY (`ID_DIsciplina`)
);

CREATE TABLE `Notas` (
	`ID_Notas` INT (10) NOT NULL,
    `Nota_1` DECIMAL (4,2),
    `Nota_2` DECIMAL (4,2),
    `Nota_3` DECIMAL (4,2),
    PRIMARY KEY (`ID_Notas`)
);

CREATE TABLE `Frequência` (
	 `ID_Frequência` INT(3) NOT NULL,
      `Frequência` INT(3),
	PRIMARY KEY (`ID_Frequência`)

);

CREATE TABLE `Cursa_Disciplinas` (
	`ID` INT(12),
    `Matricula` INT(8) NOT NULL,
    `ID_Disciplina` INT(12) NOT NULL,
    `Turma` INT(1) NOT NULL,
    PRIMARY KEY (`ID`)

);

CREATE TABLE `Empregado` (
    `Matricula` INT(8) NOT NULL,
    `ID_Cargo` INT(1) NOT NULL,
    PRIMARY KEY (`Matricula`)
);

CREATE TABLE `Cargos` (
    `ID_Cargo` INT(1) NOT NULL auto_increment,
    `Cargo` VARCHAR(30) NOT NULL,
    `Setor` VARCHAR(30) NOT NULL,
	`Salário` DECIMAL(6 , 2 ),
    PRIMARY KEY (`ID_Cargo`)
);

ALTER TABLE `Endereço` ADD CONSTRAINT `Endereço_fk0` FOREIGN KEY (`ID_Endereço`) REFERENCES `Usuário`(`ID_Endereço`);

ALTER TABLE `Contato` ADD CONSTRAINT `Contato_fk0` FOREIGN KEY (`ID_Contato`) REFERENCES `Usuário`(`ID_Contato`);

ALTER TABLE `Empregado` ADD CONSTRAINT `Empregado_fk0` FOREIGN KEY (`Matricula`) REFERENCES `Usuário`(`Matricula`);

ALTER TABLE `Responsável` ADD CONSTRAINT `Responsável_fk0` FOREIGN KEY (`Matricula`) REFERENCES `Usuário`(`Matricula`);

ALTER TABLE `Aluno` ADD CONSTRAINT `Responsável_fk1` FOREIGN KEY (`Matricula`) REFERENCES `Responsável`(`ID_Dependente`);

ALTER TABLE `Aluno` ADD constraint `Aluno_fk0` FOREIGN KEY (`ID_Responsável`) REFERENCES `Responsável`(`Matricula`);

ALTER TABLE `Aluno` ADD CONSTRAINT `Aluno_fk2` FOREIGN KEY (`ID_Disciplinas`) REFERENCES `Disciplinas`(`ID_DIsciplina`);

ALTER TABLE `Disciplinas` ADD CONSTRAINT `Disciplinas_fk1` FOREIGN KEY (`Professor`) REFERENCES `Usuário`(`Matricula`);

ALTER TABLE `Empregado` ADD CONSTRAINT `Cargos_fk0` FOREIGN KEY (`ID_Cargo`) REFERENCES `Cargos`(`ID_Cargo`);

ALTER TABLE `Cursa_Disciplinas` ADD CONSTRAINT `Cursa_Disciplinas_fk0` FOREIGN KEY (`Matricula`) REFERENCES `Aluno`(`Matricula`);

ALTER TABLE `Cursa_Disciplinas` ADD CONSTRAINT `Cursa_Disciplinas_fk1` FOREIGN KEY (`ID_Disciplina`) REFERENCES `Disciplinas`(`ID_DIsciplina`);

ALTER TABLE `Responsável` ADD CONSTRAINT `Responsável_fk2`  FOREIGN KEY (`ID_Dependente`) REFERENCES `Aluno`(`Matricula`);

ALTER TABLE `Disciplinas` ADD CONSTRAINT `Disciplinas_fk2`  FOREIGN KEY (`ID_Nota`)  REFERENCES `Notas` (`ID_Notas`);
  
ALTER TABLE `Disciplinas` ADD CONSTRAINT `Disciplinas_fk3`  FOREIGN KEY (`ID_Frequência`)  REFERENCES `Frequência` (`ID_Frequência`);



  




